# License: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of `delarte` (https://git.afpy.org/fcode/delarte.git)

"""Provide target muxing utilities."""

import subprocess


def mux_target(target, _progress):
    """Multiplexes target into a single file."""
    cmd = ["ffmpeg", "-hide_banner"]

    # inputs
    cmd.extend(["-i", target.video_input.url])
    cmd.extend(["-i", target.audio_input.url])
    if target.subtitles_input:
        cmd.extend(["-i", target.subtitles_input.url])

    # codecs
    cmd.extend(["-c:v", "copy"])
    cmd.extend(["-c:a", "copy"])
    if target.subtitles_input:
        cmd.extend(["-c:s", "copy"])

    cmd.extend(["-bsf:a", "aac_adtstoasc"])

    # stream metadata & disposition
    # cmd.extend(["-metadata:s:v:0", f"name={target.video.name!r}"])
    # cmd.extend(["-metadata:s:v:0", f"language={target.video.language!r}"])

    cmd.extend(["-metadata:s:a:0", f"name={target.audio_input.track.name}"])
    cmd.extend(["-metadata:s:a:0", f"language={target.audio_input.track.language}"])

    a_disposition = "default"
    if target.audio_input.track.original:
        a_disposition += "+original"
    else:
        a_disposition += "-original"

    if target.audio_input.track.visual_impaired:
        a_disposition += "+visual_impaired"
    else:
        a_disposition += "-visual_impaired"

    cmd.extend(["-disposition:a:0", a_disposition])

    if target.subtitles_input:
        cmd.extend(["-metadata:s:s:0", f"name={target.subtitles_input.track.name}"])
        cmd.extend(
            ["-metadata:s:s:0", f"language={target.subtitles_input.track.language}"]
        )

        s_disposition = "default"

        if target.subtitles_input.track.hearing_impaired:
            s_disposition += "+hearing_impaired+descriptions"
        else:
            s_disposition += "-hearing_impaired-descriptions"

        cmd.extend(["-disposition:s:0", s_disposition])

    # file metadata
    if isinstance(target.title, tuple):
        cmd.extend(["-metadata", f"title={target.title[0]}"])
        cmd.extend(["-metadata", f"subtitle={target.title[1]}"])
    else:
        cmd.extend(["-metadata", f"title={target.title}"])

    # output
    cmd.append(f"{target.output}.mkv")

    print(cmd)

    subprocess.run(cmd)
