# License: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of `delarte` (https://git.afpy.org/fcode/delarte.git)

"""Provide common utilities."""


class ModuleError(Exception):
    """Module error."""

    def __str__(self):
        """Use the class definition docstring as a string representation."""
        return self.__doc__

    def __repr__(self):
        """Use the class qualified name and constructor arguments."""
        return f"{self.__class__}{self.args!r}"


class ExpectedError(ModuleError):
    """A feature limitation to submit as an enhancement to developers."""


class UnexpectedError(ModuleError):
    """An error to report to developers."""


class HTTPError(Exception):
    """A wrapper around a filed HTTP response."""

    @classmethod
    def raise_for_status(self, r):
        if not 200 <= r.status < 300:
            raise self(r)


#
# www
#
class PageNotFound(ModuleError):
    """Page not found at ArteTV."""


class PageNotSupported(ExpectedError):
    """The page you are trying to download from is not (yet) supported."""


class InvalidPage(UnexpectedError):
    """Invalid ArteTV page."""


#
# api
#
class UnexpectedAPIResponse(UnexpectedError):
    """Unexpected response from ArteTV."""


#
# hls
#
class UnexpectedHLSResponse(UnexpectedError):
    """Unexpected response from ArteTV."""


class UnsupportedHLSProtocol(ModuleError):
    """Program type not supported."""


#
# subtitles
#
class WebVTTError(UnexpectedError):
    """Unexpected WebVTT data."""
