# License: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of `delarte` (https://git.afpy.org/fcode/delarte.git)

"""Provide contextualized based file naming utility."""
import re


def file_name_builder(
    *,
    use_id=False,
    sep=" - ",
    seq_pfx=" - ",
    seq_no_pad=False,
    add_rendition=False,
    add_variant=False
):
    """Create a file namer."""

    def sub_sequence_counter(match):
        index = match[1]
        if not seq_no_pad:
            index = (len(match[2]) - len(index)) * "0" + index

        return seq_pfx + index

    def replace_sequence_counter(s: str) -> str:
        return re.sub(r"\s+\((\d+)/(\d+)\)", sub_sequence_counter, s)

    def build_file_name(program, rendition, variant):
        """Create a file name."""
        if use_id:
            return program.id

        fields = [replace_sequence_counter(program.title)]
        if program.subtitle:
            fields.append(replace_sequence_counter(program.subtitle))

        if add_rendition:
            fields.append(rendition.code)

        if add_variant:
            fields.append(variant.code)

        name = sep.join(fields)
        name = re.sub(r'[/:<>"\\|?*]', "", name)

        return name

    return build_file_name
