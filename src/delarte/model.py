# License: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of `delarte` (https://git.afpy.org/fcode/delarte.git)

"""Provide data model types."""


from typing import NamedTuple, Optional


#
# Metadata objects
#
class Program(NamedTuple):
    """A program metadata."""

    id: str
    language: str
    title: str
    subtitle: str


class Rendition(NamedTuple):
    """A program rendition metadata."""

    code: str
    label: str


class Variant(NamedTuple):
    """A program variant metadata."""

    code: str
    average_bandwidth: int


#
# Track objects
#
class VideoTrack(NamedTuple):
    """A video track."""

    width: int
    height: int
    frame_rate: float


class AudioTrack(NamedTuple):
    """An audio track."""

    name: str
    language: str
    original: bool
    visual_impaired: bool


class SubtitlesTrack(NamedTuple):
    """A subtitles track."""

    name: str
    language: str
    hearing_impaired: bool


#
# Source objects
#
class ProgramSource(NamedTuple):
    """A program source item."""

    program: Program
    player_config_url: str


class RenditionSource(NamedTuple):
    """A rendition source item."""

    program: Program
    rendition: Rendition
    protocol: str
    program_index_url: Program


class VariantSource(NamedTuple):
    """A variant source item."""

    class VideoMedia(NamedTuple):
        """A video media."""

        track: VideoTrack
        track_index_url: str

    class AudioMedia(NamedTuple):
        """An audio media."""

        track: AudioTrack
        track_index_url: str

    class SubtitlesMedia(NamedTuple):
        """A subtitles media."""

        track: SubtitlesTrack
        track_index_url: str

    program: Program
    rendition: Rendition
    variant: Variant
    video_media: VideoMedia
    audio_media: AudioMedia
    subtitles_media: Optional[SubtitlesMedia]


class Target(NamedTuple):
    """A download target item."""

    class VideoInput(NamedTuple):
        """A video input."""

        track: VideoTrack
        url: str

    class AudioInput(NamedTuple):
        """An audio input."""

        track: AudioTrack
        url: str

    class SubtitlesInput(NamedTuple):
        """A subtitles input."""

        track: SubtitlesTrack
        url: str

    video_input: VideoInput
    audio_input: AudioInput
    subtitles_input: Optional[SubtitlesInput]
    title: str | tuple[str, str]
    output: str
