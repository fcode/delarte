VBIN=${VIRTUAL_ENV}/bin

help: # Print help on Makefile
	@grep '^[^.#]\+:\s\+.*#' Makefile | \
	sed "s/\(.\+\):\s*\(.*\) #\s*\(.*\)/`printf "\e[1;33;4;40m"`\1`printf "\033[0m"`	\3/" | \
	expand -t20

clean: # Remove files not tracked in source control
	find . -type f -name "*.orig" -delete
	find . -type f -name "*.pyc" -delete
	find . -type d -name "__pycache__" -delete
	find . -type d -empty -delete

format: # Format the code and lint it
	${VBIN}/black *.py
	.git/hooks/pre-commit

init-pre_commit: # Set up git pre-commit hook
	echo "make --no-print-directory --quiet lint" > .git/hooks/pre-commit && chmod u+x .git/hooks/pre-commit

lint: # Lint code
	${VBIN}/black --quiet --check *.py && echo "✅ black" || echo "🚨 black"
	${VBIN}/pydocstyle && echo "✅ pydocstyle"  || echo "🚨 pydocstyle"

open_all: # Open all projects files
	${EDITOR} ${VBIN}/activate
	${EDITOR} .gitignore delarte.py Makefile README.md requirements-dev.txt requirements.txt
	${EDITOR} .git/hooks/p*-commit

pre_commit: # Run the pre-commit hook
	bash .git/hooks/pre-commit
